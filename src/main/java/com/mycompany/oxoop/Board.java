/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mycompany.oxoop;

/**
 *
 * @author 66955
 */
public class Board {

    private char table[][] = {{'-', '-', '-'}, {'-', '-', '-'}, {'-', '-', '-'}};
    private Player currentPlyer;
    private Player o;
    private Player x;
    private int count;
    public boolean win = false;
    public boolean draw = false;

    public Board(Player o, Player x) {
        this.o = o;
        this.x = x;
        this.currentPlyer = o;
        this.count = 0;
    }

    public char[][] getTable() {
        return table;
    }

    public Player getCurrentPlyer() {
        return currentPlyer;
    }

    public Player getO() {
        return o;
    }

    public Player getX() {
        return x;
    }

    public int getCount() {
        return count;
    }

    private void switchPlayer() {
        if (currentPlyer == o) {
            currentPlyer = x;
        } else {
            currentPlyer = o;
        }
    }

    public boolean isWin() {
        return win;
    }

    public boolean isDraw() {
        return draw;
    }

    public boolean setRowCol(int row, int col) {
        if (row > 3 || col > 3 || row < 1 || col < 1) {
            return false;
        }
        if (table[row - 1][col - 1] != '-') {
            return false;
        }
        table[row - 1][col - 1] = currentPlyer.getSymbol();
        if (checkWin(row, col)) {
            updateStat();
            this.win = true;
            return true;
        }
        if (checkDraw()) {
            o.Draw();
            x.Draw();
            this.draw = true;
            return true;
        }
        count++;
        switchPlayer();
        return true;
    }

    private void updateStat() {
        if (this.currentPlyer == o) {
            o.Win();
            x.Lose();
        } else {
            o.Win();
            x.Lose();
        }
    }

    public boolean checkWin(int row, int col) {
        if (checkCol(row)) {
            return true;
        } else if (checkRow(col)) {
            return true;
        } else if (checkX1(table, currentPlyer.getSymbol())) {
            return true;
        } else if (checkX2(table, currentPlyer.getSymbol())) {
            return true;
        }

        return false;
    }

    public boolean checkRow(int col) {
        for (int r = 0; r < table.length; r++) {
            if (table[r][col - 1] != currentPlyer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkCol(int row) {
        for (int c = 0; c < table.length; c++) {
            if (table[row - 1][c] != currentPlyer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX1(char[][] table, char curentPlyer) { //11, 22 ,33
        for (int i = 0; i < table.length; i++) {
            if (table[i][i] != currentPlyer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkX2(char[][] table, char curentPlyer) { //13, 22 ,31
        for (int i = 0; i < table.length; i++) {
            if (table[i][2 - i] != currentPlyer.getSymbol()) {
                return false;
            }
        }
        return true;
    }

    public boolean checkDraw() {
        if (count == 8) {
            return true;
        }
        return false;
    }
}
